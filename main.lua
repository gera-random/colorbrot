function colordelta(c1, c2, steps)
    return {(c2[1]-c1[1])/steps, (c2[2]-c1[2])/steps,(c2[3]-c1[3])/steps, (c2[4]-c1[4])/steps}
end

function colorsum(c1, c2)
    local res = {}
    for i = 1,4 do
	res[i] = c1[i]+c2[i]
    end
    return res
end

function makepalette(c1, c2, c3, c4, p2, p3)
    palette = {c1}
    local dc = colordelta(c1,c2,p2)
    for i = 2,p2 do
	palette[i] = colorsum(palette[i-1],dc)
    end
    dc = colordelta(c2,c3,p3-p2)
    for i = p2+1,p3 do
	palette[i] = colorsum(palette[i-1],dc)
    end
    dc = colordelta(c3,c4,palette_size-p3)
    for i = p3+1,palette_size do
	palette[i] = colorsum(palette[i-1],dc)
    end
end

function makecanvas()
    canvas = love.graphics.newCanvas(ww, wh)
end

function love.load()
    ww = love.graphics.getWidth()
    wh = love.graphics.getHeight()
    makecanvas()
    
    xmax = 0.5
    xmin = -1.5
    ymax = 1
    ymin = -1
    cx = 0
    cy = 0
    sx = 1
    sy = 1
    ds = 0.01
    moving = true
    maxiter = 100
    palette_size = maxiter
    makepalette({1,0.24,0.69,1}, {0.078,0.722,0.941,1}, {0.941,0.318,0.09,1}, {0,0,0,1.0}, maxiter*0.4, maxiter*0.8)
end

function calcfractalbounds()
    local dx = scale(-cx, ww*sx, xmax, xmin)
    local dy = scale(-cy, wh*sy, ymax, ymin)
    local dx2 = scale(-cx+ww, ww*sx, xmax, xmin)
    local dy2 = scale(-cy+wh, wh*sy, ymax, ymin)
    xmin = dx
    ymin = dy
    xmax = dx2
    ymax = dy2
end

function drawfractal()
    love.graphics.setCanvas(canvas)
    love.graphics.setBlendMode("alpha")
    for x = 0,ww do
	for y = 0,wh do
	    local x0 = scale(x, ww, xmax, xmin)
	    local y0 = scale(y, wh, ymax, ymin)
	    local x2 = 0
	    local y2 = 0
	    local w = 0
	    local i = 0
	    while (x2+y2<=4 and i<maxiter) do
		local x1 = x2 - y2 + x0
		local y1 = w - x2 - y2 + y0
		x2 = x1*x1
		y2 = y1*y1
		w = (x1 + y1) * (x1 + y1)
		i = i+1
	    end
	    -- love.graphics.setColor(0, 0, i/20, 1)
	    love.graphics.setColor(palette[math.min(palette_size, i+1)])
	    love.graphics.points(x, y)
	end
    end
    love.graphics.setCanvas()
end

function scale(x, w, xmax, xmin)
    return x * (xmax - xmin) / w + xmin
end

function drawcanvas()
    love.graphics.setBlendMode("alpha", "premultiplied")
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.draw(canvas, cx, cy, 0, sx, sy)
end

function calccanvasscaleandcoords(mx, my, ds)
    local old_sx = sx
    local old_sy = sy
    sx = sx + sx*ds
    sy = sy + sy*ds
    cx = (mx - ((mx - cx)/old_sx) * sx)
    cy = (my - ((my - cy)/old_sy) * sy)
end

function love.resize(w, h)
    ww = w
    wh = h
    makecanvas()
    drawfractal()
end

function love.draw()
    drawcanvas()
    love.graphics.setBlendMode("alpha")
    love.graphics.print("xmin: "..xmin,0,0)
    love.graphics.print("ymin: "..ymin,0,20)
    love.graphics.print("xmax: "..xmax,0,40)
    love.graphics.print("ymax: "..xmax,0,60)

    local touches = love.touch.getTouches()
    local nt = #touches
    if nt == 1 then -- zoom in
	moving = true
	local id = touches[1]
	local mx, my = love.touch.getPosition(id)
	calccanvasscaleandcoords(mx, my, ds)
    elseif nt == 2 or love.mouse.isDown(2) then -- zoom out
	moving = true
	calccanvasscaleandcoords(ww/2, wh/2, -ds)
    elseif love.mouse.isDown(1) then
	moving = true
	local mx, my = love.mouse.getPosition()
	calccanvasscaleandcoords(mx, my, ds)
    else -- zoom released
	if moving then
	    calcfractalbounds() -- recalc xmin, xmax, ymin, ymax
	    drawfractal() -- recompute fractal in new bounds
	    sx = 1
	    sy = 1
	    cx = 0
	    cy = 0
	end
	moving = false
    end
end
